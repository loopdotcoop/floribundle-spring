importScripts('floribundle-spring.min.js')

onmessage = function (e) {
  console.log('js-worker.js: Got from main script:',e)
  postMessage(self.LDC.fs.greet_js_worker()+' '+e.data[0]+' '+e.data[1])
}