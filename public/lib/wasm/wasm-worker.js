import { default as wasm, greet_wasm_worker } from './floribundle_spring.js'

onmessage = function (e) {
  console.log('wasm-worker.js: Placeholder listener got from main script:',e)
  postMessage('wasm-worker.js: Placeholder: '+e.data[0]+' '+e.data[1])
}

wasm().then(module => {
  console.log('wasm-worker.js: the wasm has loaded')
  onmessage = function (e) {
    console.log('wasm-worker.js: Real listener got from main script:',e)
    postMessage(greet_wasm_worker()+' '+e.data[0]+' '+e.data[1])
  }
})
