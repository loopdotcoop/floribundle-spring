!function(G){
  // Define some simple utility functions, and some handy refs to prototypes.
  let noop = _ => {}
  let me = function () { return this }
  let A = Array.prototype
  let N = Number.prototype
  let O = Object.prototype
  let S = String.prototype
  let ctx = G.CanvasRenderingContext2D
  let C = ctx ? ctx.prototype : {}

  // Define a class which acts a bit like Rust’s `String` type.
  class RustString {
    constructor (str) { this.value = str }
    push_str (str) { this.value += str }
    toString () { return this.value }
    get length () { return this.value.length }
    chars () { return this } // eg for `line.chars().enumerate()`
    collect () { return this }
    count () { return this.value.length }
    enumerate() { return Object.entries(this.value.split('')) }
    parse() { return parseInt(this.value) }
    replace(from, to) {
      this.value = this.value.replace(new RegExp(from,'g'), to)
      return this;
    }
    skip(index) { return new RustString(this.value.slice(index)) }
    slice(from, to) { return new RustString(this.value.slice(from, to)) }
    // slice(from, to) { this.value = this.value.slice(from, to); return this }
    // skip(index) { this.value = this.value.slice(index); return this }
    take(index) { this.value = this.value.slice(0, index); return this }
    // take(index) { return new RustString(this.value.slice(0, index)) }
  }

  // Modify JS’s `Array` type, so it looks a bit more like Rust.
  A.len = function () { return this.length }
  A.to_vec = me
  A.iter = me

  // Modify JS’s `Number` types.
  N.to_string = function () { return new RustString(this+'') }
  N.unwrap = me
  N.cos = function () { return Math.cos(this) }
  N.sin = function () { return Math.sin(this) }
  
  // Modify JS’s `String` type.
  S.to_string = function () { return new RustString(this+'') }
  S.into = me

  // Modify JS’s `Object` type.
  O.clone = me

  // Modify JS’s canvas context.
  C.begin_path     = C.beginPath || noop
  C.clear_rect     = C.clearRect || noop
  C.fill_text      = C.fillText  || noop
  C.line_to        = C.lineTo    || noop
  C.move_to        = C.moveTo    || noop
  C.set_fill_style = function(v) { this.fillStyle = v }
  C.set_image_smoothing_enabled = function(v) { this.imageSmoothingEnabled = v }
  C.set_line_width = function(v) { this.lineWidth = v }
  C.set_stroke_style = function(v) { this.strokeStyle = v }
  C.clone = me

  // Usage:
  // let foo = "Foo".to_string()
  // console.log('`foo` is ', foo)
  // foo.push_str("Bar")
  // console.log('After `foo.push_str("Bar")`, `foo` is ', foo)
  // console.log(`And stringified, \`foo\` is "${foo}"`)
  // console.log('`foo.chars().enumerate()` is ', foo.chars().enumerate())
  // for (let [p, c] of foo.chars().skip(2).enumerate()) { console.log(p, c) }

}(this)
