!function(
  G, // the window (or global, or self) object
  L, // the name of the library, 'LDC'
  N  // the name of the namespace, 'fs'
){
// Create the LDC.fs namespace, if it does not exist.
G[L]=G[L]||{V:'m0.0.3'},G[L][N]=G[L][N]||{}

// Add the `Hitzone` class to the LDC.fs namespace.
G[L][N].Hitzone = class {
  constructor ({ t0, t1 }) {
    this.t0 = t0
    this.t1 = t1
  }
}

// Add the `Cell` class to the LDC.fs namespace.
G[L][N].Cell = class {
  constructor ({ hitzone }) {
    this.hitzone = hitzone
  }
}

// Add the `Spiralgrid` class to the LDC.fs namespace.
G[L][N].Spiralgrid = class {

  constructor ({
    // ctx, // web_sys::CanvasRenderingContext2d
    renderer, // Renderer
    start, // f64
    x, // f64
    y, // f64
    radius, // f64
    num_points, // f64
    pow, // f64
  }) {
    // this.ctx = ctx;
    this.renderer = renderer;
    this.start = start;
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.num_points = num_points;
    this.pow = pow;
  }

  render(now, _w, _h, focused) {const self = this;
    let color = "#00f";
    if (focused) { color = "#f00"; }
    let dy = Math.pow((now - self.start) / 1000.0, 2.0);
    let cells = generate_small_cell(self.x, self.y + dy);
    self.renderer.draw_cells(cells, color.into());
  }

}

function generate_small_cell(x, y) { let Point = G[L][N].Point, Triangle = G[L][N].Triangle, Cell = G[L][N].Cell, Hitzone = G[L][N].Hitzone;
  let a = new Point({ x, y });
  let b = new Point({ x, y:y+10.0 });
  let c = new Point({ x:x-10.0, y:y+5.0 });
  let t0 = new Triangle({ a, b, c });
  c = new  Point({ x:x+10.0, y:y+5.0 });
  let t1 = new Triangle({ a, b, c });
  return [
    new Cell({
      hitzone: new Hitzone({ t0, t1 })
    })
  ].to_vec();
}

}(typeof global=='object'?global:typeof self=='object'?self:this,'LDC','fs')
