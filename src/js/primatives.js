!function(
  G, // the window (or global, or self) object
  L, // the name of the library, 'LDC'
  N  // the name of the namespace, 'fs'
){
// Create the LDC.fs namespace, if it does not exist.
G[L]=G[L]||{V:'m0.0.3'},G[L][N]=G[L][N]||{}

// Add the `Point` class to the LDC.fs namespace.
G[L][N].Point = class {
  constructor ({ x, y }) {
    this.x = x
    this.y = y
  }
}

// Add the `Triangle` class to the LDC.fs namespace.
G[L][N].Triangle = class {
  constructor ({ a, b, c }) {
    this.a = a
    this.b = b
    this.c = c
  }
}

}(typeof global=='object'?global:typeof self=='object'?self:this,'LDC','fs')
