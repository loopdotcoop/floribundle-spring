!function(
  G, // the window (or global, or self) object
  L, // the name of the library, 'LDC'
  N  // the name of the namespace, 'fs'
){
// Create the LDC.fs namespace, if it does not exist.
G[L]=G[L]||{V:'m0.0.3'},G[L][N]=G[L][N]||{}

const RendererKind  = {
  Canvas2D: 'Canvas2D',
  WebGL: 'WebGL',
  WebGPU: 'WebGPU',
};

// Add the `World` class to the LDC.fs namespace.
G[L][N].World = class {

  constructor (
    w, // f64
    h, // f64
    canvas_id, // String
    renderer_kind, // RendererKind
    // num_points, // f64
    // pow, // f64
  ) {const renderers = G[L][N].renderers;

    // Initialise the renderer.
    let renderer;
    switch (renderer_kind) {
      case RendererKind.Canvas2D:
        renderer = new renderers.Canvas2DRenderer(canvas_id); break;
      case RendererKind.WebGL:
        renderer = new renderers.Renderer(canvas_id); break;
      case RendererKind.WebGPU:
        renderer = new renderers.Renderer(canvas_id); break;
    };

    // let spirals = [];
    // spirals.push(
    //   new G[L][N].Spiral(w, h, canvas_id)
    // );

    this.w = w;
    this.h = h;
    // this.ctx = ctx;
    this.renderer = renderer;
    this.now = 0.0,
    // this.num_points = num_points;
    // this.pow = pow;
    // this.target_num_points = num_points;
    // this.target_pow = pow;
    // this.spirals = spirals;
    this.spiralgrids = [];
    this.focused_spiralgrid = 0;
  }

  add_spiralgrid(
    x, // f64
    y, // f64
    radius, // f64
    num_points, // f64
    pow, // f64
  ) {const self = this; const Spiralgrid = G[L][N].Spiralgrid;
    self.focused_spiralgrid = self.spiralgrids.len();
    self.spiralgrids.push(
      new Spiralgrid({ renderer:self.renderer.clone(), start:self.now, x, y, radius, num_points, pow })
    );
  }

  set_now(now) {
    this.now = now;
  }

  // tween_num_points_to(target_value) {
  //   this.target_num_points = target_value;
  // }

  // tween_pow_to(target_value) {
  //   this.target_pow = target_value;
  // }

  render() {const self = this;
    self.renderer.clear_rect(0.0, 0.0, self.w, self.h);

    // // Step towards the target num_points and pow, if necessary.
    // if (self.num_points != self.target_num_points) {
    //   // Jump the last step of a tween, to prevent tiny tweens.
    //   if (self.num_points < self.target_num_points + 0.005
    //     && self.num_points > self.target_num_points - 0.005) {
    //     self.num_points = self.target_num_points
    //   } else {
    //     self.num_points = (self.num_points * 9.0 + self.target_num_points) / 10.0;
    //   }
    // }
    // if (self.pow != self.target_pow) {
    //   if (self.pow < self.target_pow + 0.005
    //     && self.pow > self.target_pow - 0.005) {
    //       self.pow = self.target_pow
    //   } else {
    //     self.pow = (self.pow * 9.0 + self.target_pow) / 10.0;
    //   }
    // }

    // // Render the Spirals.
    // for (const spiral of self.spirals) {
    //   spiral.render(self.now, self.num_points, self.pow);
    // }

    // Render the Spiralgrids.
    for (let i=0, spiralgrid=this.spiralgrids[0]; i<this.spiralgrids.length; i++, spiralgrid=this.spiralgrids[i]) {
      let focused = self.focused_spiralgrid == i;
      spiralgrid.render(self.now, self.w, self.h, focused);
    }
  }

}

G[L][N].greet_js_worker = function() {
  return "Hello, js-worker!".to_string();
}

}(typeof global=='object'?global:typeof self=='object'?self:this,'LDC','fs')
