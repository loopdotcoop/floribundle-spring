!function(
  G, // the window (or global, or self) object
  L, // the name of the library, 'LDC'
  N  // the name of the namespace, 'fs'
){
// Create the LDC.fs namespace, if it does not exist.
G[L]=G[L]||{V:'m0.0.3'},G[L][N]=G[L][N]||{}

const PHI = 1.6180339887498948482;

// Add the `Spiral` class to the LDC.fs namespace.
G[L][N].Spiral = class {

  constructor (w, h, canvas_id, num_points, pow) {
    let document = G.document;
    let canvas = document.getElementById(canvas_id);
    let ctx = canvas.getContext("2d");
    ctx.set_image_smoothing_enabled(true);
    ctx.set_line_width(0.5);
    ctx.set_stroke_style("#0698".into());

    this.w = w;
    this.h = h;
    this.ctx = ctx;
    this._num_points = num_points;
    this._pow = pow;
    this.cells = regenerate_cells(num_points, pow);
  }

  render(now, num_points, pow) {const self = this;
    let cx = self.w / 2.0;
    let cy = self.h / 2.0;
    let cycle = ~~(now / 2000.0);

    self.ctx.clear_rect(0.0, 0.0, self.w, self.h);

    let turn_fraction = PHI;
    let num_points_minus_1 = num_points - 1.0;
    let scale = self.w * 0.4;
    let highlight_offset = cycle * -5;
    let highlight = 8;

    let highlighted_coords = [];
    let points = [];
    let a_triangles = [];
    let b_triangles = [];
    let c_triangles = [];
    let d_triangles = [];

    for (let i=0; i<num_points; i++) {
      let f = i;
      let dst = Math.pow(f / num_points_minus_1, pow);
      let angle = 2.0 * Math.PI * turn_fraction * f;
      let x = dst * angle.cos();
      let y = dst * angle.sin();
      if ((i + highlight_offset) % highlight == 0) {
        highlighted_coords.push([x, y]);
      }
      points.push([x * scale + cx, y * scale + cy]);
    }

    if (num_points >= 3) {
      for (let i=0; i<num_points; i++) {
        if (i == 0) { continue; } // looks weird to draw a triangle here
        let i0 = i;
        let i1 = i + 13;
        let i2 = i + 21;
        if (i1 >= num_points) { continue; } // no triangle for outermost points
        if (i2 >= num_points) { continue; } // no triangle for outermost points
        if (i1 <= 0) { i1 = 0; }
        if (i2 <= 0) { i2 = 0; }
        // console.log(i, highlight_offset, highlight, (i + highlight_offset) % highlight);
        if ((i + highlight_offset) % highlight == 0) {
          c_triangles.push([points[i0], points[i1], points[i2]]);
        } else {
          a_triangles.push([points[i0], points[i1], points[i2]]);
        }
      }
      for (let i=0; i<num_points; i++) {
        if (i == 0) { continue; } // looks weird to draw a triangle here
        let i0 = i;
        let i1 = i + 8;
        let i2 = i + 21;
        if (i1 >= num_points) { continue; } // no triangle for outermost points
        if (i2 >= num_points) { continue; } // no triangle for outermost points
        if (i1 <= 0) { i1 = 0; }
        if (i2 <= 0) { i2 = 0; }
        if ((i + highlight_offset) % highlight == 0) {
          d_triangles.push([points[i0], points[i1], points[i2]]);
        } else {
          b_triangles.push([points[i0], points[i1], points[i2]]);
        }
      }
    }

    self.ctx.begin_path();
    self.ctx.set_fill_style("#ba1".into());
    for (let triangle of a_triangles) {
      self.ctx.move_to(triangle[0][0], triangle[0][1]);
      self.ctx.line_to(triangle[1][0], triangle[1][1]);
      self.ctx.line_to(triangle[2][0], triangle[2][1]);
      self.ctx.line_to(triangle[0][0], triangle[0][1]);
    }
    self.ctx.fill();

    self.ctx.begin_path();
    self.ctx.set_fill_style("#cc2".into());
    for (let triangle of b_triangles) {
      self.ctx.move_to(triangle[0][0], triangle[0][1]);
      self.ctx.line_to(triangle[1][0], triangle[1][1]);
      self.ctx.line_to(triangle[2][0], triangle[2][1]);
      self.ctx.line_to(triangle[0][0], triangle[0][1]);
    }
    self.ctx.fill();

    self.ctx.begin_path();
    self.ctx.set_fill_style("#c93".into());
    for (let triangle of c_triangles) {
      self.ctx.move_to(triangle[0][0], triangle[0][1]);
      self.ctx.line_to(triangle[1][0], triangle[1][1]);
      self.ctx.line_to(triangle[2][0], triangle[2][1]);
      self.ctx.line_to(triangle[0][0], triangle[0][1]);
    }
    self.ctx.fill();

    self.ctx.begin_path();
    self.ctx.set_fill_style("#d93".into());
    for (let triangle of d_triangles) {
      self.ctx.move_to(triangle[0][0], triangle[0][1]);
      self.ctx.line_to(triangle[1][0], triangle[1][1]);
      self.ctx.line_to(triangle[2][0], triangle[2][1]);
      self.ctx.line_to(triangle[0][0], triangle[0][1]);
    }
    self.ctx.fill();

    for (let cell of self.cells.iter()) {
      self.ctx.begin_path();
      self.ctx.set_fill_style("#f00".into());
      self.ctx.move_to(cell.hitzone.t0.a.x, cell.hitzone.t0.a.y);
      self.ctx.line_to(cell.hitzone.t0.b.x, cell.hitzone.t0.b.y);
      self.ctx.line_to(cell.hitzone.t0.c.x, cell.hitzone.t0.c.y);
      self.ctx.line_to(cell.hitzone.t0.a.x, cell.hitzone.t0.a.y);
      self.ctx.fill();
      self.ctx.begin_path();
      self.ctx.set_fill_style("#f30".into());
      self.ctx.move_to(cell.hitzone.t1.a.x, cell.hitzone.t1.a.y);
      self.ctx.line_to(cell.hitzone.t1.b.x, cell.hitzone.t1.b.y);
      self.ctx.line_to(cell.hitzone.t1.c.x, cell.hitzone.t1.c.y);
      self.ctx.line_to(cell.hitzone.t1.a.x, cell.hitzone.t1.a.y);
      self.ctx.fill();
    }
  }
  
}

function regenerate_cells(_num_points/*: i32*/, _pow/*: f64*/)/* -> Vec<Cell>*/ {
  const { Point, Triangle, Hitzone, Cell } = G[L][N];
  let a = new Point({ x:10.0, y:10.0 });
  let b = new Point({ x:50.0, y:10.0 });
  let c = new Point({ x:10.0, y:50.0 });
  let t0 = new Triangle({ a, b, c });
  a = new Point({ x:50.0, y:50.0 });
  let t1 = new Triangle({ a, b, c });
  return [
    new Cell({
      hitzone: new Hitzone({ t0, t1 })
    })
  ].to_vec();
}

}(typeof global=='object'?global:typeof self=='object'?self:this,'LDC','fs')
