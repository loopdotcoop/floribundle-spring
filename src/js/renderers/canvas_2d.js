!function(
  G, // the window (or global, or self) object
  L, // the name of the library, 'LDC'
  N, // the name of the namespace, 'fs'
  S, // the name of the subnamespace, 'renderers'
){
// Create the LDC.fs.renderers namespace, if it does not exist.
G[L]=G[L]||{V:'m0.0.3'},G[L][N]=G[L][N]||{},G[L][N][S]=G[L][N][S]||{}

function init_canvas_2d (canvas_id) {
  let document = G.document;
  let canvas = document.getElementById(canvas_id);
  let ctx = canvas.getContext("2d");
  ctx.set_image_smoothing_enabled(true);
  ctx.set_line_width(0.5);
  ctx.set_stroke_style("#0698".into());
  return ctx;
}


// Add the `Canvas2DRenderer` class to the LDC.fs namespace.
G[L][N][S].Canvas2DRenderer = class {

  constructor (
    canvas_id, // String
  ) {
    let ctx = init_canvas_2d(canvas_id);
    this.ctx = ctx;
  }

  draw_cells (cells/*:Vec<Cell>*/, color/*:String*/) { let self = this;
    self.ctx.begin_path();
    self.ctx.set_fill_style(color.into());
    for (let cell of cells) {
      let t0 = cell.hitzone.t0;
      let t1 = cell.hitzone.t1;
      self.ctx.move_to(t0.a.x, t0.a.y);
      self.ctx.line_to(t0.b.x, t0.b.y);
      self.ctx.line_to(t0.c.x, t0.c.y);
      self.ctx.line_to(t0.a.x, t0.a.y);
      self.ctx.line_to(t1.b.x, t1.b.y);
      self.ctx.line_to(t1.c.x, t1.c.y);
      self.ctx.line_to(t1.a.x, t1.a.y);
    }
    self.ctx.fill();
  }

  clear_rect (x/*:f64*/, y/*:f64*/, w/*:f64*/, h/*:f64*/) { let self = this;
    self.ctx.clear_rect(x, y, w, h);
  }

}


}(typeof global=='object'?global:typeof self=='object'?self:this,
'LDC','fs','renderers')
