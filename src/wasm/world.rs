use std::f64;
use wasm_bindgen::prelude::*;

// use crate::spiral::*;
use crate::renderers::canvas_2d::*;
use crate::spiralgrid::*;

#[wasm_bindgen]
pub enum RendererKind {
  Canvas2D,
  WebGL,
  WebGPU,
}

#[wasm_bindgen]
pub struct World {
  w: f64,
  h: f64,
  // ctx: web_sys::CanvasRenderingContext2d,
  renderer: Renderer,
  now: f64,
  // num_points: f64, // stored here as f64 for smoother tweening
  // pow: f64,
  // target_num_points: f64,
  // target_pow: f64,
  // spirals: Vec<Spiral>,
  spiralgrids: Vec<Spiralgrid>,
  focused_spiralgrid: usize,
}

#[wasm_bindgen]
impl World {
  pub fn new(
    w: f64,
    h: f64,
    canvas_id: String,
    renderer_kind: RendererKind,
    // num_points: f64,
    // pow: f64,
  ) -> World {

    // Initialise the renderer.
    let renderer = match renderer_kind {
      RendererKind::Canvas2D => Renderer::new(canvas_id),
      RendererKind::WebGL => Renderer::new(canvas_id),
      RendererKind::WebGPU => Renderer::new(canvas_id),
    };

    // let mut spirals = Vec::new();
    // spirals.push(
    //   Spiral::new(w, h, canvas_id, num_points as i32, pow)
    // );

    World {
      w,
      h,
      // ctx,
      renderer,
      now: 0.0,
      // num_points,
      // pow,
      // target_num_points: num_points,
      // target_pow: pow,
      // spirals,
      spiralgrids: Vec::new(),
      focused_spiralgrid: 0,
    }
  }

  pub fn add_spiralgrid(
    &mut self,
    x: f64,
    y: f64,
    radius: f64,
    num_points: f64,
    pow: f64,
  ) {
    self.focused_spiralgrid = self.spiralgrids.len();
    self.spiralgrids.push(
      Spiralgrid { renderer:self.renderer.clone(), start:self.now, x, y, radius, num_points, pow }
    );
  }

  pub fn set_now(&mut self, now: f64) {
    self.now = now;
  }

  // pub fn tween_num_points_to(&mut self, target_value: f64) {
  //   self.target_num_points = target_value;
  // }

  // pub fn tween_pow_to(&mut self, target_value: f64) {
  //   self.target_pow = target_value;
  // }

  #[allow(unused_parens)]
  pub fn render(&mut self) {
    self.renderer.clear_rect(0.0, 0.0, self.w, self.h);

    // // Step towards the target num_points and pow, if necessary.
    // if (self.num_points != self.target_num_points) {
    //   // Jump the last step of a tween, to prevent tiny tweens.
    //   if (self.num_points < self.target_num_points + 0.005
    //     && self.num_points > self.target_num_points - 0.005) {
    //     self.num_points = self.target_num_points
    //   } else {
    //     self.num_points = (self.num_points * 9.0 + self.target_num_points) / 10.0;
    //   }
    // }
    // if (self.pow != self.target_pow) {
    //   if (self.pow < self.target_pow + 0.005
    //     && self.pow > self.target_pow - 0.005) {
    //       self.pow = self.target_pow
    //   } else {
    //     self.pow = (self.pow * 9.0 + self.target_pow) / 10.0;
    //   }
    // }

    // // Render the Spirals.
    // for spirals in &self.spirals {
    //   spirals.render(self.now, self.num_points as i32, self.pow);
    // }

    // Render the Spiralgrids.
    for (i, spiralgrid) in self.spiralgrids.iter().enumerate() {
      let focused = self.focused_spiralgrid == i;
      spiralgrid.render(self.now, self.w, self.h, focused);
    }

  }

}
