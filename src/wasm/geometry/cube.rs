use crate::primative3d::*;

pub fn build_cube(
  side_length: f64,
) -> Vec<Triangle3D> {
  let half = side_length / 2.0;
  [
    Triangle3D {
      a: Point3D { x: -half, y: -half, z:0.0 },
      b: Point3D { x:  half, y: -half, z:0.0 },
      c: Point3D { x: -half, y:  half, z:0.0 },
    },
    Triangle3D {
      a: Point3D { x:  half, y: -half, z:0.0 },
      b: Point3D { x: -half, y:  half, z:0.0 },
      c: Point3D { x:  half, y:  half, z:0.0 },
    },
  ].to_vec()
}
