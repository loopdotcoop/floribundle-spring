use crate::primative3d::*;
use crate::geometry::*;

#[derive(Clone)]
pub struct Geometry {
  kind: GeometryKind,
  side_length: f64,
  pub triangles: Vec<Triangle3D>,
}

impl Geometry {

  pub fn new(
    kind: GeometryKind,
    side_length: f64,
  ) -> Self {
    let triangles = match kind {
      GeometryKind::Cube => build_cube(side_length),
      GeometryKind::Tetrahedron => build_tetrahedron(side_length),
    };

    Geometry {
      kind,
      side_length,
      triangles,
    }
  }

  pub fn to_json(&self) -> String {
    let mut out = "{ \"side_length\":".to_string();
    out.push_str(&self.side_length.to_string());
    out.push_str(" }");
    out
  }
}
