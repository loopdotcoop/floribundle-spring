mod geometry;

mod cube;
mod tetrahedron;

pub use geometry::Geometry;

pub use cube::build_cube;
pub use tetrahedron::build_tetrahedron;

#[derive(Clone)]
pub enum GeometryKind {
  Cube,
  Tetrahedron,
}
