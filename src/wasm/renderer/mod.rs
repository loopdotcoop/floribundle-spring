mod renderer;
mod canvas2d;
mod webgl;

pub use renderer::*;
pub use canvas2d::*;
pub use webgl::*;

pub enum RendererKind {
  Canvas2D,
  WebGL,
}
