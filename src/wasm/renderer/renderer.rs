use crate::renderer::*;
use crate::object3d::{Camera,Scene};

#[derive(Clone)]
pub struct CtxOptions {
  pub canvas2d: Option<web_sys::CanvasRenderingContext2d>,
  pub webgl: Option<web_sys::WebGlRenderingContext>,
}

pub struct Renderer {
  kind: RendererKind,
  w: f64,
  h: f64,
  canvas_id: String,
  ctx_options: CtxOptions,
}

impl Renderer {

  pub fn new(
    kind: RendererKind,
    w: f64,
    h: f64,
    canvas_id: String,
  ) -> Self {
    let ctx_options: CtxOptions = match kind {
      RendererKind::Canvas2D => init_canvas2d(canvas_id.clone()),
      RendererKind::WebGL => init_webgl(canvas_id.clone()),
    };
    Renderer {
      kind,
      w,
      h,
      canvas_id,
      ctx_options,
    }
  }

  pub fn render(&self, scene: Scene, camera: Camera) {
    let w = self.w;
    let h = self.h;
    let ctx = self.ctx_options.clone();
    match self.kind {
      RendererKind::Canvas2D => render_canvas2d(w, h, scene, camera, ctx),
      RendererKind::WebGL => render_webgl(w, h, scene, camera, ctx),
    };
  }

  pub fn to_json(&self) -> String {
    let mut out = "{ \"w\":".to_string();
    out.push_str(&self.w.to_string());
    out.push_str(", \"h\":");
    out.push_str(&self.h.to_string());
    out.push_str(", \"canvas_id\":\"");
    out.push_str(&self.canvas_id.to_string());
    out.push_str("\" }");
    out
  }

}
