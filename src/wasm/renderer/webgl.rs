use wasm_bindgen::JsCast;
use web_sys::{WebGlProgram, WebGlRenderingContext, WebGlShader};

use crate::renderer::CtxOptions;
use crate::object3d::{Camera,Scene};

pub fn init_webgl (canvas_id: String) -> CtxOptions {
  let document = web_sys::window().unwrap().document().unwrap();
  let canvas = document.get_element_by_id(&canvas_id).unwrap();
  let canvas: web_sys::HtmlCanvasElement = canvas
    .dyn_into::<web_sys::HtmlCanvasElement>()
    .map_err(|_| ())
    .unwrap();
  let context = canvas
    .get_context("webgl")
    .unwrap()
    .unwrap()
    .dyn_into::<web_sys::WebGlRenderingContext>()
    .unwrap();

  //rustwasm.github.io/wasm-bindgen/examples/webgl.html
  let vert_shader = compile_shader(
    &context,
    WebGlRenderingContext::VERTEX_SHADER,
    r#"
    attribute vec4 position;
    void main() {
      gl_Position = position;
    }
  "#,
  );
  let frag_shader = compile_shader(
    &context,
    WebGlRenderingContext::FRAGMENT_SHADER,
    r#"
    void main() {
      gl_FragColor = vec4(0.0, 1.0, 1.0, 1.0);
    }
  "#,
  );
  let program = link_program(&context, &vert_shader.unwrap(), &frag_shader.unwrap());
  context.use_program(Some(&program.unwrap()));

  return CtxOptions {
    canvas2d: None,
    webgl: Some(context),
  };
}

pub fn render_webgl(
  _w: f64,
  _h: f64,
  scene: Scene,
  _camera: Camera,
  ctx_options: CtxOptions
) {
  match ctx_options.webgl {
    None => return,
    Some(context) => draw_vertices(scene, context),
  }
}

pub fn compile_shader(
  context: &WebGlRenderingContext,
  shader_type: u32,
  source: &str,
) -> Result<WebGlShader, String> {
  let shader = context
    .create_shader(shader_type)
    .ok_or_else(|| String::from("Unable to create shader object"))?;
  context.shader_source(&shader, source);
  context.compile_shader(&shader);

  if context
    .get_shader_parameter(&shader, WebGlRenderingContext::COMPILE_STATUS)
    .as_bool()
    .unwrap_or(false)
  {
    Ok(shader)
  } else {
    Err(context
      .get_shader_info_log(&shader)
      .unwrap_or_else(|| String::from("Unknown error creating shader")))
  }
}

pub fn link_program(
  context: &WebGlRenderingContext,
  vert_shader: &WebGlShader,
  frag_shader: &WebGlShader,
) -> Result<WebGlProgram, String> {
  let program = context
    .create_program()
    .ok_or_else(|| String::from("Unable to create shader object"))?;

  context.attach_shader(&program, vert_shader);
  context.attach_shader(&program, frag_shader);
  context.link_program(&program);

  if context
    .get_program_parameter(&program, WebGlRenderingContext::LINK_STATUS)
    .as_bool()
    .unwrap_or(false)
  {
    Ok(program)
  } else {
    Err(context
      .get_program_info_log(&program)
      .unwrap_or_else(|| String::from("Unknown error creating program object")))
  }
}


fn draw_vertices(
  scene: Scene,
  context: WebGlRenderingContext,
) {
  let mut vertices: Vec<f32> = [].to_vec();
  for mesh in scene.meshes {
    for triangle in mesh.geometry.triangles {
      vertices.push((mesh.x + triangle.a.x) as f32);
      vertices.push((- mesh.y - triangle.a.y) as f32);
      vertices.push((mesh.z + triangle.a.z) as f32);
      vertices.push((mesh.x + triangle.b.x) as f32);
      vertices.push((- mesh.y - triangle.b.y) as f32);
      vertices.push((mesh.z + triangle.b.z) as f32);
      vertices.push((mesh.x + triangle.c.x) as f32);
      vertices.push((- mesh.y - triangle.c.y) as f32);
      vertices.push((mesh.z + triangle.c.z) as f32);
    }
  }

  // let vertices: [f32; 9] = [-0.7, -0.7, 0.0, 0.7, -0.7, 0.0, 0.0, 0.7, 0.0];

  let buffer = context.create_buffer().ok_or("failed to create buffer");
  context.bind_buffer(WebGlRenderingContext::ARRAY_BUFFER, Some(&buffer.unwrap()));

  // Note that `Float32Array::view` is somewhat dangerous (hence the
  // `unsafe`!). This is creating a raw view into our module's
  // `WebAssembly.Memory` buffer, but if we allocate more pages for ourself
  // (aka do a memory allocation in Rust) it'll cause the buffer to change,
  // causing the `Float32Array` to be invalid.
  //
  // As a result, after `Float32Array::view` we have to be very careful not to
  // do any memory allocations before it's dropped.
  unsafe {
    let vert_array = js_sys::Float32Array::view(&vertices);

    context.buffer_data_with_array_buffer_view(
      WebGlRenderingContext::ARRAY_BUFFER,
      &vert_array,
      WebGlRenderingContext::STATIC_DRAW,
    );
  }

  context.vertex_attrib_pointer_with_i32(
    0, 3, WebGlRenderingContext::FLOAT, false, 0, 0);
  context.enable_vertex_attrib_array(0);

  context.clear_color(0.0, 0.0, 0.0, 0.0);
  context.clear(WebGlRenderingContext::COLOR_BUFFER_BIT);

  context.draw_arrays(
    WebGlRenderingContext::TRIANGLES,
    0,
    (vertices.len() / 3) as i32,
  );

}
