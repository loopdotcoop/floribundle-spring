use wasm_bindgen::JsCast;

use crate::renderer::CtxOptions;
use crate::object3d::{Camera,Scene};
use crate::primative3d::Triangle3D;

// struct Canvas2DCtx {
//   canvas2d_ctx: web_sys::CanvasRenderingContext2d,
// }

pub fn init_canvas2d (canvas_id: String) -> CtxOptions {
  let document = web_sys::window().unwrap().document().unwrap();
  let canvas = document.get_element_by_id(&canvas_id).unwrap();
  let canvas: web_sys::HtmlCanvasElement = canvas
      .dyn_into::<web_sys::HtmlCanvasElement>()
      .map_err(|_| ())
      .unwrap();
  let ctx = canvas
      .get_context("2d")
      .unwrap()
      .unwrap()
      .dyn_into::<web_sys::CanvasRenderingContext2d>()
      .unwrap();
  ctx.set_image_smoothing_enabled(true);
  ctx.set_line_width(0.5);
  ctx.set_stroke_style(&"#0698".into());
  return CtxOptions {
    canvas2d: Some(ctx),
    webgl: None,
  };
}

pub fn render_canvas2d(
  w: f64,
  h: f64,
  scene: Scene,
  _camera: Camera,
  ctx_options: CtxOptions
) {
  match ctx_options.canvas2d {
    None => return,
    Some(ctx) => for mesh in scene.meshes {
      ctx.begin_path();
      let color = mesh.material.color;
      ctx.set_fill_style(&color.into());
      for triangle in mesh.geometry.triangles {
        draw_triangle(
          w,
          h,
          mesh.x,
          mesh.y,
          mesh.z,
          ctx.clone(),
          triangle,
        );
      }
      ctx.fill();
    }
  }
}

fn draw_triangle (
  w: f64,
  h: f64,
  x: f64,
  y: f64,
  _z: f64,
  ctx: web_sys::CanvasRenderingContext2d,
  triangle: Triangle3D
) {
  // Replicate WebGL coordinates:
  ctx.move_to(w * 0.5 * (triangle.a.x + x + 1.0), h * 0.5 * (triangle.a.y + y + 1.0));
  ctx.line_to(w * 0.5 * (triangle.b.x + x + 1.0), h * 0.5 * (triangle.b.y + y + 1.0));
  ctx.line_to(w * 0.5 * (triangle.c.x + x + 1.0), h * 0.5 * (triangle.c.y + y + 1.0));
  ctx.line_to(w * 0.5 * (triangle.a.x + x + 1.0), h * 0.5 * (triangle.a.y + y + 1.0));
}
