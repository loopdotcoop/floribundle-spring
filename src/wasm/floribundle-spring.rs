use wasm_bindgen::prelude::*;

mod geometry;
mod material;
mod object3d;
mod primative3d;
mod renderer;

mod main;

mod primatives;
mod renderers;
mod spiralgrid;
mod spiral;
mod world;

pub use world::World;
pub use main::Main;

#[wasm_bindgen]
pub fn greet_wasm_worker() -> String {
  return "Hello, wasm-worker!".to_string();
}


/*
trait Lepidoptera {
  fn style(&self) -> String;
}

pub struct Moth {
  desc: String,
}

pub struct Butterfly {
  desc: String,
}

impl Lepidoptera for Moth {
  fn style(&self) -> String {
    self.desc.to_string()
  }
}

fn new_moth (desc: String) -> Moth {
  Moth { desc }
}

impl Lepidoptera for Butterfly {
  fn style(&self) -> String {
    self.desc.to_string()
  }
}

fn new_butterfly (desc: String) -> Butterfly {
  Butterfly { desc }
}


#[wasm_bindgen]
pub fn moth_style() -> String {
  // let moth = Moth::new();
  let moth = Moth { desc:"Earthy".into() };
  Moth::style(&moth)
}

#[wasm_bindgen]
pub fn butterfly_style() -> String {
  // let butterfly = Butterfly::new();
  let butterfly = Butterfly { desc:"Bright".into() };
  butterfly.style()
}

fn is_moth(lep: &dyn Lepidoptera) -> bool {
  let result: String = lep.style();
  result == "Earthy"
}

#[wasm_bindgen]
pub fn gather_leps() -> String {
  let moth1 = new_moth("Earthy".into());
  let moth2 = Moth { desc:"Earthy".into() };
  let butterfly1 = new_butterfly("Dazzling".into());
  
  let leps: Vec<&dyn Lepidoptera> = vec![&moth1,&moth2,&butterfly1];
  let mut out = "".to_string();
  for lep in leps {
    if is_moth(lep) {
      out.push_str("moth ");
    } else {
      out.push_str("butterfly ");
    }
  }
  out
}
*/