use std::f64;

#[derive(Clone, Copy)]
pub struct Point {
  pub x: f64,
  pub y: f64,
}

#[derive(Clone, Copy)]
pub struct Triangle {
  pub a: Point,
  pub b: Point,
  pub c: Point,
}

// pub struct AudioSnippet {
//   name: str
// }