use wasm_bindgen::prelude::*;

use crate::geometry::*;
use crate::material::*;
use crate::object3d::*;
use crate::renderer::*;

#[wasm_bindgen]
pub struct Main {
  camera: Camera,
  renderer: Renderer,
  scene: Scene,
}

#[wasm_bindgen]
impl Main {

  pub fn new(
    w: f64,
    h: f64,
    canvas_id: String,
    renderer_kind_str: &str,
  ) -> Self {
    let camera = Camera::new();
    let renderer_kind = match renderer_kind_str {
      "Canvas2D" => RendererKind::Canvas2D,
      "WebGL" => RendererKind::WebGL,
      _ => RendererKind::WebGL, // default
    };

    let renderer = Renderer::new(
      renderer_kind,
      w,
      h,
      canvas_id,
    );
    let scene = Scene::new();
    Main {
      camera,
      renderer,
      scene,
    }
  }

  pub fn render(&self) {
    self.renderer.render(
      self.scene.clone(),
      self.camera.clone(),
    );
  }

  pub fn to_json(&self) -> String {
    let mut out = "{ \"camera\":".to_string();
    out.push_str(&self.camera.to_json());
    out.push_str(", \"renderer\":");
    out.push_str(&self.renderer.to_json());
    out.push_str(", \"scene\":");
    out.push_str(&self.scene.to_json());
    out.push_str("}");
    out
  }

  pub fn add_cube(
    &mut self,
    x: f64,
    y: f64,
    z: f64,
    side_length: f64,
    color: String,
  ) {
    let geometry = Geometry::new(GeometryKind::Cube, side_length);
    let material = Material::new(color);
    let mesh = Mesh::new(geometry, material, x, y, z);
    self.scene.meshes.push(mesh);
  }

  pub fn add_tetrahedron(
    &mut self,
    x: f64,
    y: f64,
    z: f64,
    side_length: f64,
    color: String,
  ) {
    let geometry = Geometry::new(GeometryKind::Tetrahedron, side_length);
    let material = Material::new(color);
    let mesh = Mesh::new(geometry, material, x, y, z);
    self.scene.meshes.push(mesh);
  }

}
