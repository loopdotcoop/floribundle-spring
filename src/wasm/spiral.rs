use std::f64;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

use crate::primatives::*;
use crate::spiralgrid::*;

const PHI: f64 = 1.6180339887498948482;

#[wasm_bindgen]
pub struct Spiral {
  w: f64,
  h: f64,
  ctx: web_sys::CanvasRenderingContext2d,
  _num_points: i32, // along with pow, defines what `cells` must contain
  _pow: f64, // along with num_points, defines what `cells` must contain
  cells: Vec<Cell>
}

#[wasm_bindgen]
impl Spiral {

  pub fn new(
    w: f64,
    h: f64,
    canvas_id: String,
    num_points: i32, // defines the initial condition
    pow: f64 // defines the initial condition
  ) -> Spiral {
    let document = web_sys::window().unwrap().document().unwrap();
    let canvas = document.get_element_by_id(&canvas_id).unwrap();
    let canvas: web_sys::HtmlCanvasElement = canvas
        .dyn_into::<web_sys::HtmlCanvasElement>()
        .map_err(|_| ())
        .unwrap();
    let ctx = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();
    ctx.set_image_smoothing_enabled(true);
    ctx.set_line_width(0.5);
    ctx.set_stroke_style(&"#0698".into());
    Spiral {
      w,
      h,
      ctx,
      _num_points: num_points,
      _pow: pow,
      cells: regenerate_cells(num_points, pow)
    }
  }

  #[allow(unused_parens)]
  pub fn render(&self, now: f64, num_points: i32, pow: f64) {
    let cx = self.w / 2.0;
    let cy = self.h / 2.0;
    let cycle = (now / 2000.0) as i32;

    self.ctx.clear_rect(0.0, 0.0, self.w, self.h);

    let turn_fraction = PHI;
    let num_points_minus_1 = num_points as f64 - 1.0;
    let scale = self.w * 0.4;
    let highlight_offset = cycle * -5;
    let highlight = 8;

    let mut highlighted_coords: Vec<(f64, f64)> = [].to_vec();
    let mut points: Vec<(f64, f64)> = [].to_vec();
    let mut a_triangles: Vec<((f64, f64), (f64, f64), (f64, f64))> = [].to_vec();
    let mut b_triangles: Vec<((f64, f64), (f64, f64), (f64, f64))> = [].to_vec();
    let mut c_triangles: Vec<((f64, f64), (f64, f64), (f64, f64))> = [].to_vec();
    let mut d_triangles: Vec<((f64, f64), (f64, f64), (f64, f64))> = [].to_vec();

    for i in 0..num_points {
      let f = i as f64;
      let dst = f64::powf(f / num_points_minus_1, pow);
      let angle = 2.0 * f64::consts::PI * turn_fraction * f;
      let x = dst * angle.cos();
      let y = dst * angle.sin();
      if ((i + highlight_offset) % highlight == 0) {
        highlighted_coords.push((x, y));
      }
      points.push((x * scale + cx, y * scale + cy));
    }

    if (num_points >= 3) {
      for i in 0..num_points {
        if (i == 0) { continue; } // looks weird to draw a triangle here
        let i0 = i;
        let mut i1 = i + 13;
        let mut i2 = i + 21;
        if (i1 >= num_points) { continue; } // no triangle for outermost points
        if (i2 >= num_points) { continue; } // no triangle for outermost points
        if (i1 <= 0) { i1 = 0; }
        if (i2 <= 0) { i2 = 0; }
        if ((i + highlight_offset) % highlight == 0) {
          c_triangles.push((points[i0 as usize], points[i1 as usize], points[i2 as usize]));
        } else {
          a_triangles.push((points[i0 as usize], points[i1 as usize], points[i2 as usize]));
        }
      }
      for i in 0..num_points {
        if (i == 0) { continue; } // looks weird to draw a triangle here
        let i0 = i;
        let mut i1 = i + 8;
        let mut i2 = i + 21;
        if (i1 >= num_points) { continue; } // no triangle for outermost points
        if (i2 >= num_points) { continue; } // no triangle for outermost points
        if (i1 <= 0) { i1 = 0; }
        if (i2 <= 0) { i2 = 0; }
        if ((i + highlight_offset) % highlight == 0) {
          d_triangles.push((points[i0 as usize], points[i1 as usize], points[i2 as usize]));
        } else {
          b_triangles.push((points[i0 as usize], points[i1 as usize], points[i2 as usize]));
        }
      }
    }

    self.ctx.begin_path();
    self.ctx.set_fill_style(&"#ba1".into());
    for triangle in a_triangles {
      self.ctx.move_to(triangle.0.0, triangle.0.1);
      self.ctx.line_to(triangle.1.0, triangle.1.1);
      self.ctx.line_to(triangle.2.0, triangle.2.1);
      self.ctx.line_to(triangle.0.0, triangle.0.1);
    }
    self.ctx.fill();

    self.ctx.begin_path();
    self.ctx.set_fill_style(&"#cc2".into());
    for triangle in b_triangles {
      self.ctx.move_to(triangle.0.0, triangle.0.1);
      self.ctx.line_to(triangle.1.0, triangle.1.1);
      self.ctx.line_to(triangle.2.0, triangle.2.1);
      self.ctx.line_to(triangle.0.0, triangle.0.1);
    }
    self.ctx.fill();

    self.ctx.begin_path();
    self.ctx.set_fill_style(&"#c93".into());
    for triangle in c_triangles {
      self.ctx.move_to(triangle.0.0, triangle.0.1);
      self.ctx.line_to(triangle.1.0, triangle.1.1);
      self.ctx.line_to(triangle.2.0, triangle.2.1);
      self.ctx.line_to(triangle.0.0, triangle.0.1);
    }
    self.ctx.fill();

    self.ctx.begin_path();
    self.ctx.set_fill_style(&"#d93".into());
    for triangle in d_triangles {
      self.ctx.move_to(triangle.0.0, triangle.0.1);
      self.ctx.line_to(triangle.1.0, triangle.1.1);
      self.ctx.line_to(triangle.2.0, triangle.2.1);
      self.ctx.line_to(triangle.0.0, triangle.0.1);
    }
    self.ctx.fill();

    for cell in self.cells.iter() {
      self.ctx.begin_path();
      self.ctx.set_fill_style(&"#f00".into());
      self.ctx.move_to(cell.hitzone.t0.a.x, cell.hitzone.t0.a.y);
      self.ctx.line_to(cell.hitzone.t0.b.x, cell.hitzone.t0.b.y);
      self.ctx.line_to(cell.hitzone.t0.c.x, cell.hitzone.t0.c.y);
      self.ctx.line_to(cell.hitzone.t0.a.x, cell.hitzone.t0.a.y);
      self.ctx.fill();
      self.ctx.begin_path();
      self.ctx.set_fill_style(&"#f30".into());
      self.ctx.move_to(cell.hitzone.t1.a.x, cell.hitzone.t1.a.y);
      self.ctx.line_to(cell.hitzone.t1.b.x, cell.hitzone.t1.b.y);
      self.ctx.line_to(cell.hitzone.t1.c.x, cell.hitzone.t1.c.y);
      self.ctx.line_to(cell.hitzone.t1.a.x, cell.hitzone.t1.a.y);
      self.ctx.fill();
    }

  }

}

fn regenerate_cells(_num_points: i32, _pow: f64) -> Vec<Cell> {
  let a = Point { x:10.0, y:10.0 };
  let b = Point { x:50.0, y:10.0 };
  let c = Point { x:10.0, y:50.0 };
  let t0 = Triangle { a, b, c };
  let a = Point { x:50.0, y:50.0 };
  let t1 = Triangle { a, b, c };
  return [
    Cell {
      hitzone: Hitzone { t0, t1 }
    }
  ].to_vec();
}
