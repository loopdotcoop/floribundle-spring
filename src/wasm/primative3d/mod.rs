#[derive(Clone, Copy)]
pub struct Point3D {
  pub x: f64,
  pub y: f64,
  pub z: f64,
}

#[derive(Clone, Copy)]
pub struct Triangle3D {
  pub a: Point3D,
  pub b: Point3D,
  pub c: Point3D,
}
