use std::f64;

use crate::primatives::*;
use crate::renderers::canvas_2d::*;

#[derive(Clone, Copy)]
pub struct Hitzone {
  pub t0: Triangle,
  pub t1: Triangle
}

#[derive(Clone, Copy)]
pub struct Cell {
  // audioSnippet: AudioSnippet,
  pub hitzone: Hitzone
}

pub struct Spiralgrid {
  // pub ctx: web_sys::CanvasRenderingContext2d,
  pub renderer: Renderer,
  pub start: f64,
  pub x: f64,
  pub y: f64,
  pub radius: f64,
  pub num_points: f64,
  pub pow: f64,
}

#[allow(unused_parens)]
impl Spiralgrid {
  pub fn render(&self, now: f64, _w: f64, _h: f64, focused: bool) {
    let mut color = "#00f";
    if (focused) { color = "#f00"; }
    let dy = f64::powf((now - self.start) / 1000.0, 2.0);
    let cells = generate_small_cell(self.x, self.y + dy);
    self.renderer.draw_cells(cells, color.into());
  }
}

fn generate_small_cell(x:f64, y:f64) -> Vec<Cell> {
  let a = Point { x, y };
  let b = Point { x, y:y+10.0 };
  let c = Point { x:x-10.0, y:y+5.0 };
  let t0 = Triangle { a, b, c };
  let c = Point { x:x+10.0, y:y+5.0 };
  let t1 = Triangle { a, b, c };
  return [
    Cell {
      hitzone: Hitzone { t0, t1 }
    }
  ].to_vec();
}
