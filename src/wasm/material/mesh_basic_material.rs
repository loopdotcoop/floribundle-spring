pub struct MeshBasicMaterial {
  color: String,
}

impl MeshBasicMaterial {
  pub fn new(
    color: String,
  ) -> Self {
    MeshBasicMaterial {
      color,
    }
  }
}
