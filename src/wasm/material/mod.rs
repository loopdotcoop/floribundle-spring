// pub mod mesh_basic_material;

#[derive(Clone)]
pub struct Material {
  pub color: String,
}

impl Material {

  pub fn new(
    color: String,
  ) -> Self {
    Material {
      color,
    }
  }

  pub fn to_json(&self) -> String {
    let mut out = "{ \"color\": \"".to_string();
    out.push_str(&self.color);
    out.push_str("\" }");
    out
  }
}
