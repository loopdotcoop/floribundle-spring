use wasm_bindgen::JsCast;

use crate::spiralgrid::Cell;
use crate::renderers::Renderer;

#[derive(Clone)]
pub struct WebGLRenderer  {
  ctx: web_sys::CanvasRenderingContext2d,
}

fn init_canvas_2d (canvas_id: String) -> web_sys::CanvasRenderingContext2d {
  let document = web_sys::window().unwrap().document().unwrap();
  let canvas = document.get_element_by_id(&canvas_id).unwrap();
  let canvas: web_sys::HtmlCanvasElement = canvas
      .dyn_into::<web_sys::HtmlCanvasElement>()
      .map_err(|_| ())
      .unwrap();
  let ctx = canvas
      .get_context("2d")
      .unwrap()
      .unwrap()
      .dyn_into::<web_sys::CanvasRenderingContext2d>()
      .unwrap();
  ctx.set_image_smoothing_enabled(true);
  ctx.set_line_width(0.5);
  ctx.set_stroke_style(&"#0698".into());
  return ctx;
}

pub fn new_web_gl_renderer (canvas_id: String) -> WebGLRenderer {
  let ctx = init_canvas_2d(canvas_id);
  WebGLRenderer { ctx }
}

impl Renderer for WebGLRenderer {
  // fn new (canvas_id: String) -> Self {
  //   let ctx = init_canvas_2d(canvas_id);
  //   WebGLRenderer { ctx }
  // }
  
  fn draw_cells (&self, cells:Vec<Cell>, color:String) {
    self.ctx.begin_path();
    self.ctx.set_fill_style(&color.into());
    for cell in cells {
      let t0 = cell.hitzone.t0;
      let t1 = cell.hitzone.t1;
      self.ctx.move_to(t0.a.x, t0.a.y);
      self.ctx.line_to(t0.b.x, t0.b.y);
      self.ctx.line_to(t0.c.x, t0.c.y);
      self.ctx.line_to(t0.a.x, t0.a.y);
      self.ctx.line_to(t1.b.x, t1.b.y);
      self.ctx.line_to(t1.c.x, t1.c.y);
      self.ctx.line_to(t1.a.x, t1.a.y);
    }
    self.ctx.fill();
  }

  fn clear_rect (&self, x:f64, y:f64, w:f64, h:f64) {
    self.ctx.clear_rect(x, y, w, h);
  }
}
