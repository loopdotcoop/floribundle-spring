use crate::object3d::*;

#[derive(Clone)]
pub struct Scene {
  pub meshes: Vec<Mesh>,
}

impl Scene {
  pub fn new() -> Self {
    Scene {
      meshes: [].to_vec(),
    }
  }

  pub fn to_json(&self) -> String {
    let mut out = "{ \"meshes\":[".to_string();
    for mesh in &self.meshes {
      out.push_str(&mesh.to_json());
      out.push_str(",");
    }
    out.pop(); // trailing comma
    out.push_str("]}");
    out
  }
}
