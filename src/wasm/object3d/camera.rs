#[derive(Clone)]
pub struct Camera {
  pub x: f64,
  pub y: f64,
  pub z: f64,
}

impl Camera {
  pub fn new() -> Self {
    Camera {
      x: 0.0,
      y: 0.0,
      z: 10.0,
    }
  }

  pub fn to_json(&self) -> String {
    let mut out = "{ \"x\":".to_string();
    out.push_str(&self.x.to_string());
    out.push_str(", \"y\":");
    out.push_str(&self.y.to_string());
    out.push_str(", \"z\":");
    out.push_str(&self.z.to_string());
    out.push_str(" }");
    out
  }
}
