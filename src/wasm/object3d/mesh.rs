use crate::geometry::Geometry;
use crate::material::Material;

#[derive(Clone)]
pub struct Mesh {
  pub geometry: Geometry,
  pub material: Material,
  pub x: f64,
  pub y: f64,
  pub z: f64,
}

impl Mesh {
  pub fn new(
    geometry: Geometry,
    material: Material,
    x: f64,
    y: f64,
    z: f64,
  ) -> Self {
    Mesh {
      geometry,
      material,
      x,
      y,
      z,
    }
  }

  pub fn to_json(&self) -> String {
    let mut out = "{ \"geometry\":".to_string();
    out.push_str(&self.geometry.to_json());
    out.push_str(", \"material\":");
    out.push_str(&self.material.to_json());
    out.push_str(", \"x\":");
    out.push_str(&self.x.to_string());
    out.push_str(", \"y\":");
    out.push_str(&self.y.to_string());
    out.push_str(", \"z\":");
    out.push_str(&self.z.to_string());
    out.push_str(" }");
    out
  }

}
